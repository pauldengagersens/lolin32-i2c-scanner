# Connection
SHT30 VIN <-- --> ESP32 3.3v  
SHT30 GND <-- --> ESP32 GND  
SHT30 SCL <-- --> ESP32 IO32: ESP32MX-E SCL  
SHT30 SDA <-- --> ESP32 IO33: ESP32MX-E SDA  

# Run
1. Clone and compile this project in Platformio
2. Load it to target ESP32 board
3. open serial monitor, you will see screen like this below  
![expected](imgs/expected.JPG)
