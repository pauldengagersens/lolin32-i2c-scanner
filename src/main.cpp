#include <Arduino.h>

#include <Wire.h>

TwoWire I2C_DEV = TwoWire(0);

#define I2C_SDA 33
#define I2C_SCL 32
 
void setup() {
  // according to https://github.com/espressif/arduino-esp32/issues/1395#issuecomment-387951003
  I2C_DEV.setClock(50000);

  // according to https://github.com/espressif/arduino-esp32/issues/1395#issuecomment-387610192
  I2C_DEV.setTimeOut(1000);
  
  I2C_DEV.begin(33, 32);
  Serial.begin(115200);
  Serial.println("\nI2C Scanner");
}
 
void loop() {
  byte error, address;
  int nDevices;
  Serial.println("Scanning...");
  nDevices = 0;
  for(address = 1; address < 0xff; address++ ) {
    I2C_DEV.beginTransmission(address);
    error = I2C_DEV.endTransmission();
    if (error == 0) {
      Serial.print("I2C device found at address 0x");
      if (address<16) {
        Serial.print("0");
      }
      Serial.println(address,HEX);
      nDevices++;
    }
    else if (error==4) {
      Serial.print("Unknow error at address 0x");
      if (address<16) {
        Serial.print("0");
      }
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0) {
    Serial.println("No I2C devices found\n");
  }
  else {
    Serial.println("done\n");
  }
  delay(5000);          
}